package entidades;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import entidades.enums.PedidoStatus;

public class Pedido {

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private static StringBuilder sb = new StringBuilder();

	private Date dataPedido;
	private PedidoStatus pedidoStatus;
	private Cliente cliente;
	private List<Item> itens = new ArrayList<>();

	public Pedido() {
	}

	public Pedido(Date dataPedido, PedidoStatus pedidoStatus, Cliente cliente) {
		this.dataPedido = dataPedido;
		this.pedidoStatus = pedidoStatus;
		this.cliente = cliente;
	}

	public Date getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public PedidoStatus getPedidoStatus() {
		return pedidoStatus;
	}

	public void setPedidoStatus(PedidoStatus pedidoStatus) {
		this.pedidoStatus = pedidoStatus;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void addItem(Item item) {
		itens.add(item);
	}

	public void removeItem(Item item) {
		itens.remove(item);
	}

	public double total() {
		double soma = 0;
		for (Item i : itens) {
			soma += i.subTotal();
		}

		return soma;
	}

	public String toString() {
		sb.append("Data do Pedido: " + sdf.format(dataPedido) + "\n");
		sb.append("Status: " + pedidoStatus + "\n");
		sb.append("Cliente: " + cliente + "\n");
		sb.append("Itens do Pedido: \n");
		for (Item i : itens) {
			sb.append(i + "\n");
		}
		sb.append("Total: " + String.format("%.2f", total()));

		return sb.toString();
	}

}
