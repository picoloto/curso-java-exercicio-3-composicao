package entidades;

public class Item {
	private Integer quantidade;
	private Produto produto;

	public Item() {
	}

	public Item(Integer quantidade, Produto produto) {
		this.quantidade = quantidade;
		this.produto = produto;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Double subTotal() {
		return produto.getPreco() * quantidade;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Item -> " + produto.getNome());
		sb.append(", Pre�o: " + String.format("%.2f", produto.getPreco()));
		sb.append(", Quantidade: " + quantidade);
		sb.append(", Subtotal: " + String.format("%.2f", subTotal()));

		return sb.toString();
	}
}
