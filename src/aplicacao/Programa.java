package aplicacao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import entidades.Cliente;
import entidades.Item;
import entidades.Pedido;
import entidades.Produto;
import entidades.enums.PedidoStatus;

public class Programa {

	public static void main(String[] args) throws ParseException {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataPedido = new Date();

		System.out.println("Dados do Cliente");
		System.out.print("Nome: ");
		String nome = sc.nextLine();
		System.out.print("Email: ");
		String email = sc.nextLine();
		System.out.print("Data de Nascimento(DD/MM/YYYY): ");
		Date nascimento = sdf.parse(sc.nextLine());

		System.out.println();

		System.out.println("Informe os dados do pedido: ");
		System.out.print("Status: ");
		String status = sc.nextLine();
		System.out.print("Quantos itens ser�o adicionados a este pedido? ");
		Integer qtdItens = sc.nextInt();
		sc.nextLine();
		
		Pedido pedido = new Pedido(dataPedido, PedidoStatus.valueOf(status.toUpperCase()), new Cliente(nome, email, nascimento));

		for (int i = 1; i <= qtdItens; i++) {
			System.out.println();
			System.out.println("Dados do pedido #" + i);
			System.out.print("Nome do Produto: ");
			String nomeProduto = sc.nextLine();
			System.out.print("Pre�o: ");
			Double preco = sc.nextDouble();
			System.out.print("Quantidade: ");
			Integer qtdProduto = sc.nextInt();
			sc.nextLine();

			pedido.addItem(new Item(qtdProduto, new Produto(nomeProduto, preco)));
		}
		
		System.out.println();
		System.out.println("Detalhes do Pedido");
		System.out.println(pedido);

		sc.close();

	}

}
